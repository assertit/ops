#!/usr/bin/env bash

apt-get update && apt-get upgrade -y

apt-get install -y curl vim

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get install -y nodejs

