#!/bin/sh
set -e

USER_NAME=$1
KEY_FILE=$2

if [ -z ${USER_NAME} ]; then
        echo "The script expects a username as input."
        exit 1
fi

if [ -z ${KEY_FILE} ]; then
        echo "Please specific path to the public key file."
        exit 1
elif [ ! -f ${KEY_FILE} ]; then
        echo "Public key file not found."
        exit 1
fi

# Create home directory and ssh dir
USER_HOME=/home/${USER_NAME}

if [ -d ${USER_HOME} ]; then
        echo "The username already exists."
        exit 1
fi

mkdir -p ${USER_HOME}/.ssh
echo "" > ${USER_HOME}/.ssh/authorized_keys

# Creating a new user
useradd -s /bin/bash  -d ${USER_HOME} ${USER_NAME}

# Give sudo to the user
usermod -aG sudo ${USER_NAME}

# Add nopass access to root
echo "\n# User rules for ${USER_NAME}" >> /etc/sudoers.d/90-cloud-init-users
echo "${USER_NAME} ALL=(ALL) NOPASSWD:ALL\n" >> /etc/sudoers.d/90-cloud-init-users

# Make the new user owner of the home dir
chown -R ${USER_NAME}:${USER_NAME} ${USER_HOME}

chmod 700 ${USER_HOME}/.ssh
chmod 644 ${USER_HOME}/.ssh/authorized_keys
cat ${KEY_FILE} >> ${USER_HOME}/.ssh/authorized_keys

echo "Successfully created: ${USER_NAME}"
