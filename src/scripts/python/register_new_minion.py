from salt_ops import SaltOps
import os
import time


class RegisterNewMinion:
    @staticmethod
    def get_pre_accept_minions():
        """
        Retrieve a list of all pre accept minions
        :return: List
        """
        wheel = SaltOps.get_wheel_client()
        data = wheel.cmd('key.list', ['pre'])

        return data['minions_pre']

    @staticmethod
    def accept_minion(minion_name):
        """
        Accepts a new minion
        :param minion_name: String
        :return: None
        """
        wheel = SaltOps.get_wheel_client()
        try:
            wheel.cmd('key.accept', [minion_name])
        except:
            raise SystemError("Error while accept minion: %s " % minion_name)

    @staticmethod
    def copy_user_files(minion_name):
        """
        Copies all relevant files ssh to minion
        :param minion_name: String
        :return: None
        """
        time.sleep(3)
        SaltOps.run(minion_name, "mkdir -p /root/.ssh/pub-keys")
        SaltOps.copy(minion_name, "/root/ops/src/scripts/shell/create_user.sh", "/root/create_user.sh")

        ssh_key_dir = "/root/.ssh/pub-keys"
        for file_name in os.listdir(ssh_key_dir):
            if file_name.strip().endswith(".id_rsa.pub"):
                    SaltOps.copy(minion_name, ssh_key_dir + "/" + file_name, "/root/.ssh/pub-keys/" + file_name)

    @staticmethod
    def add_users(minion_name):
        """
        Creates a new users on the minion
        :param minion_name: String
        :return: None
        """
        SaltOps.run(minion_name, "chmod +x /root/create_user.sh")

        ssh_key_dir = "/root/.ssh/pub-keys"
        for file_name in os.listdir(ssh_key_dir):
            if file_name.strip().endswith(".id_rsa.pub"):
                SaltOps.run(minion_name, "/root/create_user.sh %s %s" % (file_name.strip().split(".id_rsa.pub")[0],
                                                                      ssh_key_dir + "/" + file_name))

    @staticmethod
    def delete_default_user(minion_name):
        """
        Delete the default user
        :param minion_name: String
        :return: None
        """
        SaltOps.run(minion_name, "deluser --remove-home ubuntu")

    @staticmethod
    def register(minion_name):
        """
        Register a new minion and apply security
        :param minion_name:
        :return:
        """
        print "Registering new minion: %s" % minion_name
        RegisterNewMinion.accept_minion(minion_name)
        RegisterNewMinion.copy_user_files(minion_name)
        RegisterNewMinion.add_users(minion_name)
        RegisterNewMinion.delete_default_user(minion_name)

    @staticmethod
    def run():
        """
        Runs the registry
        :return: None
        """
        new_minions = RegisterNewMinion.get_pre_accept_minions()

        for minion_name in new_minions:
            RegisterNewMinion.register(minion_name)


if __name__ == '__main__':
    RegisterNewMinion.run()
