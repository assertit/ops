#!/usr/bin/env python
import subprocess


class Commons:
    @staticmethod
    def run(cmd, shell=True, verbose=False):
        """
        Runs a CLI command
        :param cmd: The command that needs to be run
        :param shell: Run as shell
        :param verbose: log inputs and outputs
        :return: Dict
        """
        status = 0
        if verbose:
            print cmd

        try:
            data = subprocess.check_output(cmd, shell=True) if shell else subprocess.check_output(cmd)
        except subprocess.CalledProcessError as e:
            data = str(e)

        return {'status': status, 'data': data}
