#!/usr/bin/env python
import salt.config
import salt.wheel
import salt.client
from commons import Commons


class SaltOps():
    @staticmethod
    def get_wheel_client():
        """
        Returns the salt-api wheel client
        :return: Object
        """
        opts = salt.config.master_config('/etc/salt/master')
        return salt.wheel.WheelClient(opts)

    @staticmethod
    def get_salt_client():
        """
        Returns the salt client
        :return: Object
        """
        return salt.client.LocalClient()

    @staticmethod
    def copy(minion, source, destination):
        """
        Copies file from salt master to salt minion
        :param minion: Name of the minion to run the command on
        :param source: Source of the file
        :param destination: Destination of file
        :return: None
        """
        cmd = "salt-cp '%s' %s %s" % (minion, source, destination)
        output = Commons.run(cmd, shell=True)

        if output['status'] != 0:
            raise SystemError("Copy failed with the following error: " + output['data'])

    @staticmethod
    def run(minion, cmd):
        """
        Runs the command on salt minion
        :param minion: Name of the minion to run the command on
        :param cmd: Command to execute on the minion
        :return: None
        """
        client = SaltOps.get_salt_client()
        try:
            return client.cmd(minion, 'cmd.run', [cmd])
        except:
            raise SystemError("Failed to run salt command: %s" % cmd)
