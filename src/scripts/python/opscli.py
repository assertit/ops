import argparse
import os
from register_new_minion import RegisterNewMinion


def register_pid():
    f = open('/var/run/register_new_minion.pid', 'w')
    try:
        pid = f.read().strip()
        os.kill(pid, 0)
    except:
        f.write(str(os.getgid()))
        f.close()
        return True
    else:
        f.close()
        return False


def register_minions_func(args):
    """
    Registers new minions
    :param args: Object
    :return: None
    """
    if register_pid():
        RegisterNewMinion.run()


def argparser():
    """
    The cli script argument parser
    :return: Object
    """
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest='cmd')

    register_minions_parser = subparsers.add_parser('register-minions', help="Used to register new minions")
    register_minions_parser.set_defaults(func=register_minions_func)

    return parser


def main():
    """
    Run the program
    :return: None
    """
    args = argparser().parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
